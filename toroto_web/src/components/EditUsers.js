import React from 'react'
import { observer, inject } from 'mobx-react'
import { Row, Col, Button, Form, Container, Alert } from 'react-bootstrap'

class EditUsers extends React.Component {
  render () {
    const { userStore } = this.props
    return (
      <Container>
        <Row>
          <Col xs={{ span: 6, offset: 3 }}>
            <Form>
              <Form.Group controlId='formBasicUsername'>
                <Form.Label>Username</Form.Label>
                <Form.Control
                  type='text' placeholder='Enter usernmae'
                  value={userStore.username}
                  onChange={(e) => { userStore.username = e.target.value }}
                />
              </Form.Group>
              <Form.Group controlId='formBasicEmail'>
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type='email' placeholder='Email'
                  value={userStore.email}
                  onChange={(e) => { userStore.email = e.target.value }}

                />
              </Form.Group>
              <Form.Group controlId='formBasicPhone'>
                <Form.Label>Phone</Form.Label>
                <Form.Control
                  type='text' placeholder='Phone'
                  value={userStore.phone}
                  onChange={(e) => { userStore.phone = e.target.value }}
                />
              </Form.Group>
              <Form.Group controlId='formBasicType'>
                <Form.Label>Type</Form.Label>
                <Form.Control
                  type='text' placeholder='Type'
                  value={userStore.type}
                  onChange={(e) => { userStore.type = e.target.value }}
                />
              </Form.Group>
              <Form.Group controlId='formBasicSubscription'>
                <Form.Label>Subscription</Form.Label>
                <Form.Control
                  type='text' placeholder='Subscription'
                  value={userStore.subscription}
                  onChange={(e) => { userStore.subscription = e.target.value }}
                />
              </Form.Group>
              <Row>
                <Col xs={6}>
                  {!userStore.EditUsers
                    ? <Button
                      block variant='warning'
                      onClick={(e) => {
                        e.preventDefault()
                        userStore.update()
                      }}
                    >Edit
                    </Button>
                    : <Button
                      block variant='primary'
                      onClick={(e) => {
                        e.preventDefault()
                        userStore.create()
                      }}
                    >Create
                    </Button>}
                </Col>
                <Col xs={6}>
                  <Button block variant='danger' onClick={() => { userStore.clear() }}>Back</Button>
                </Col>
                <Col xs={12}>
                  {userStore.userError === ''
                    ? ''
                    : <Alert variant='info'>{userStore.userError}</Alert>}
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
      </Container>
    )
  }
}

export default inject('userStore')(observer(EditUsers))
