import React from 'react'
import { Row, Col, Button, Form, Alert } from 'react-bootstrap'
import api from '../utils/sources'
import { observer, inject } from 'mobx-react'

class Login extends React.Component {
  constructor (props) {
    super(props)
    this.state = { username: '', password: '' }
  }

  handleSubmit = async (e) => {
    let {store} = this.props
    e.preventDefault()
    const { username, password } = this.state
    if (username !== '' && password !== '') {
      const login = await api.login(username, password)
      if (!login.data) {
          store.updateLogin()
          store.updateUserName(username)
      } else {
          store.updateLoginError(login.data.error)
      }
    } else {
      store.updateLoginError('Need to type username or password')
    }
    
  }

  render () {
    const { store } = this.props
    return (
      <Row className='justify-content-md-center'>
        <Col  xs={{ span: 6, offset: 3 }} lg={{span:3, offset: 0}}>
          <Form onSubmit={this.handleSubmit}>
            <Form.Group controlId='formBasicUser'>
              <Form.Label>Username</Form.Label>
              <Form.Control
                type='text' placeholder='Enter username'
                onChange={e => this.setState({ username: e.target.value })}
              />
            </Form.Group>
            <Form.Group controlId='formBasicPassword'>
              <Form.Label>Password</Form.Label>
              <Form.Control
                type='password' placeholder='Password'
                onChange={e => this.setState({ password: e.target.value })}
              />
            </Form.Group>
            <Button variant='primary' type='submit'>Submit</Button>
            {store.loginError === ''
                ? ''
                : <Alert variant='info'>{store.loginError}</Alert>
            }
          </Form>
        </Col>
      </Row>
    )
  }
}

export default inject("store")(observer(Login))
