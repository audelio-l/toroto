import React from 'react'
import { observer, inject } from 'mobx-react'
import { Col, Row, Table, Button, Container } from 'react-bootstrap'

class Users extends React.Component {
  render () {
    const { userStore } = this.props

    return (
      <Container>
        <Row>
          <Col xs={12}>
            <Table striped bordered hover variant='dark'>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Type</th>
                  <th>Subscription</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {userStore.usersList.map(user =>
                  <tr key={user.id}>
                    <td>{user.id}</td>
                    <td>{user.username}</td>
                    <td>{user.email}</td>
                    <td>{user.phone}</td>
                    <td>{user.type}</td>
                    <td>{user.Subscriptions[0].description}</td>
                    <td>
                      <Button block variant='warning' onClick={() => userStore.edit(user)}>Edit</Button>
                    </td>
                  </tr>
                )}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    )
  }
}

export default inject('userStore')(observer(Users))
