import React from 'react'
import { observer, inject } from 'mobx-react'
import Users from './Users'
import EditUsers from './EditUsers'
import { Button, Row, Col, Container, Alert } from 'react-bootstrap'

class Dashboard extends React.Component {
  render () {
    const { store, userStore } = this.props

    return (
      <Container>
        <Row>
          <Col xs lg={10}>
            <Alert variant='dark'>Bienvenido: {store.userName}</Alert>
          </Col>
          <Col xs lg={2}>
            <Button block onClick={store.logout}>Logout</Button>
          </Col>
        </Row>
        <Row xs lg={12}>
          {userStore.editUser
            ? <EditUsers />
            : <Users />}
        </Row>
      </Container>
    )
  }
}

export default inject('store', 'userStore')(observer(Dashboard))
