import React from 'react'
import { Col, Button, ButtonToolbar } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'

class Menu extends React.Component {
  render () {
    return (
      <Col>
        <h2>
          <ButtonToolbar className='custom-btn-toolbar'>
            <LinkContainer to='/dashboard'>
              <Button>dashboard</Button>
            </LinkContainer>
            <LinkContainer to='/users'>
              <Button>Users</Button>
            </LinkContainer>
          </ButtonToolbar>
        </h2>
      </Col>
    )
  }
}

export default Menu
