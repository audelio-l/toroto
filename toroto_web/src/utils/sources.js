const axios = require('axios')

const api = () => {
  axios.defaults.baseURL = process.env.API || 'https://ajdtorotoapi.herokuapp.com/api/v1'
  axios.defaults.headers.post['Content-Type'] = 'application/json'

  const getLastToken = () => {
    return window.localStorage.getItem('token')
  }

  const getUsers = async () => {
    const users = await axios.get('/users', { headers: { Authorization: getLastToken() } })
    return users
  }

  const getUser = (id) => {
    axios.get(`/users/${id}`, { headers: { Authorization: getLastToken() } })
  }

  const updateUser = (payload) => {
    console.log(payload)
    const { id } = payload
    return axios.patch(`/users/${id}`, payload,
      { headers: { Authorization: getLastToken() } })
  }

  const createUser = (payload) => {
    axios.post('/users', {
      headers: { Authorization: getLastToken() },
      boady: { payload }
    })
  }

  const login = async (username, password) => {
    const login = await axios.post('/users/login', {
      username: username, password: password
    })
    if (login.data && login.data.accessToken) {
      window.localStorage.setItem('token', login.data.accessToken)
      return true
    } else {
      return login
    }
  }

  const active = async () => {
    const active = await axios.post('/users/active', {}, {
      headers: { Authorization: getLastToken() }
    })
    return active
  }
  return {
    getUsers,
    getUser,
    updateUser,
    createUser,
    login,
    active
  }
}

export default api()
