import React from 'react'
import './App.css'

import { Jumbotron, Badge } from 'react-bootstrap'
import Dashboard from './components/Dashboard'
import Login from './components/Login'
import { Provider, observer } from 'mobx-react'
import mainStore from './stores/mainStore'
import userStore from './stores/userStore'

class App extends React.Component {
  render () {
    return (
      <Provider store={mainStore} userStore={userStore}>
        <Jumbotron style={{ 'padding-bottom': '30%' }}>
          <h1 className='header' style={{ 'text-align': 'center', padding: '30px' }}>
            <Badge variant='secondary'>Toroto Users</Badge>
          </h1>
          {!mainStore.login
            ? <Login />
            : <Dashboard />}
        </Jumbotron>
      </Provider>
    )
  }
}

export default observer(App)
