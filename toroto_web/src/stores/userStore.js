import { observable, decorate } from 'mobx'
import api from '../utils/sources'

class userStore {
    editUser = false
    username = ''
    email = ''
    phone = ''
    type = ''
    subscription = ''
    userId = ''
    subscriptionId = ''
    usersList = []
    userError = ''

    getUsers () {
        return api.getUsers().then(list => {
            this.usersList = list.data
        })
    }

    edit (user) {
        this.username = user.username
        this.email = user.email
        this.phone = user.phone
        this.type = user.type
        this.subscription = user.Subscriptions[0].description
        this.userId = user.id
        this.subscriptionId = user.Subscriptions[0].id
        this.editUser = true
    }

    clear () {
        this.editUser = false
        this.username = ''
        this.email = ''
        this.phone = ''
        this.type = ''
        this.subscription = ''
        this.userId = ''
        this.subscriptionId = ''
        this.userError = ''
    }

    update() {
        if (this.username !== ''){
            api.updateUser({
                id: this.userId,
                username: this.username,
                email: this.email,
                phone: this.phone,
                type: this.type,
                Subscriptions: [{
                    description: this.subscription,
                    id: this.subscriptionId
                }]            
            }).then(resp => {
                this.clear()
                this.getUsers()
            })
        } else {
            this.userError = 'Username is not allowed empty'
        }
    }
}

decorate(userStore, {
    username: observable,
    email: observable,
    phone: observable,
    type: observable,
    subscription: observable,
    userId: observable,
    subscriptionId: observable,
    usersList: observable,
    editUser: observable,
    userError: observable
})

const userstoreInstance = new userStore()
export default userstoreInstance
