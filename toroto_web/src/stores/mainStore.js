import { observable, action, decorate } from 'mobx'
import api from '../utils/sources'
import userStore from './userStore'

class Store {

    constructor() {
        //Check if Token Still Active
        api.active().then(() => {
            this.login = true
            userStore.getUsers()
            this.updateUserName(window.localStorage.getItem('username'))
        })        
    }

    login = false
    loginError = ''
    userName = ''

    updateLogin () {
        this.login = !this.login
        this.loginError = ''
        userStore.getUsers()
    }

    updateLoginError (error) {
        this.loginError = error
    }

    updateUserName (user) {
        this.userName = user
        window.localStorage.setItem('username', user)
    }

    logout = () => {
        this.login = false
        window.localStorage.setItem('username', '')
        window.localStorage.setItem('token', '')
    }
}

decorate(Store, {
    login: observable,
    logout: action,
    updateLogin: action,
    loginError: observable,
    updateLoginError: action,
    userName: observable,
    updateUserName: action
})

const storeInstance = new Store()
export default storeInstance;