# Toroto Users

Es un pequeño proyecto para una entrevista las instrucciones están disponibles en [`/.rules`](https://gitlab.com/audelio-l/torotoback/-/blob/master/.rules "`/.rules`"). El proyecto consta de dos repositorios:

Presentacion:
https://docs.google.com/presentation/d/1gMmtuG-FjPAw2omAqnZm97BMV8zkQUH3-Kc4y49jLmQ

Backend :
https://gitlab.com/audelio-l/torotoback/-/tree/master

Frontend:
https://gitlab.com/audelio-l/toroto

Para poder ver el DEMO puedes hacerlo desde:

Usuario: admin

Password: admin

https://ajdtorotoweb.herokuapp.com/

Y el backend:

https://ajdtorotoapi.herokuapp.com/

`Para mas información de la API favor de visitar el .README de ese repositorio.`


# ¿Como contribuir?

Para realizar cambios en el Frontend, solo basta con clonar el proyecto en tu maquina local, dirigirte al la carpeta del repositorio, dentro de esta existe `/toroto_web` que no es mas que el  create-react-app.

Para poder ponerlo en modo desarrollo solo basta con correr los siguientes comandos en tu terminal dentro de `/toroto_web`.

`yarn install`

`yarn dev`

![](https://serving.photos.photobox.com/481852827e4f65efb01483c5f29d0794eed07ccb2e73e5d65f4e7fddce4c711b05a6a5ab.jpg)

Puedes realizar los cambios que desees y hacer los commits que deseas al proyecto.

Por default la API apunta a la de demo, pero si quieres enviarla a tu local solo tienes que cambiar /toroto_web/src/utils/sources.js:
Línea 4:`localhost:3030/api/v1`
